<!doctype html>
<html lang="en">

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <style>
        .jumbotron {
            font-size: 20px;
            padding: 60px;
            background-color: #00c8eb;
            text-align: center;
            color: white;
        }
        .centered{
            margin: auto;
        }   
    </style>
    <title>@yield('title')</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
        <div class="container-fluid">
            <div class="collapse navbar-collapse" id="navbarNav">
            <a href="{{ url ('/')}}"><button class="btn btn-outline-success me-2" type="button">Home</button></a>
            <a href="{{ url ('/profile')}}"><button class="btn btn-outline-success me-2" type="button">Profile</button></a>
            <a href="{{ url ('/order')}}"><button class="btn btn-outline-success me-2" type="button">Order</button></a>
            </div>
        </div>
    </nav>

    @yield('container')



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

</body>

</html>

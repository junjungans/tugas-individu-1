@extends('template.template')

@section('title', 'Test Laravel')



@section('container')
<h1 class="mt-3" style="margin-left: 15px;">Welcome back {{$nama}}!</h1>


<table class="table table-striped table-dark centered" style="width: 50%; margin-top: 15px;">
    <thead>
        <tr>
            <th scope="col">Month</th>
            <th scope="col">Expense (Dalam juta)</th>
        </tr>
    </thead>
    <tbody>
    @foreach($order as $month => $data)
        <tr>
            <th scope="row">{{$month}}</th>
            <td>{{$data}}</td>
        </tr>
    @endforeach
</table>
@endsection

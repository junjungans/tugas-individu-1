<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/profile', function () {
    $nama = 'Junjungan Siregar';
    $order = ['Januari' => 10, 'Februari' =>20];
    return view('profile', ['nama' => $nama, 'order' => $order]);
});

Route::get('/order', function () {
    $order = ['120 gula', '50 garam', ' 90 minyak', '100 telur'];
    return view('order', ['order' => $order]);

});